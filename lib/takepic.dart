import 'dart:convert';
import 'dart:io';

import 'package:async/async.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:image/image.dart' as img;
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shakal_adwords/utils.dart';

class ImageResultWidget extends StatefulWidget {
  static const routeName = "imageResult";

  const ImageResultWidget(this.imageFileName);

  final String imageFileName;

  @override
  _ImageResultWidgetState createState() => _ImageResultWidgetState();
}

class _ImageResultWidgetState extends State<ImageResultWidget> {
  File _file;
  String _url;

  @override
  void initState() {
    super.initState();
    _file = File(widget.imageFileName);
  }

  @override
  Widget build(BuildContext context) {
    print("Result screen with file: ${widget.imageFileName}");

    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: true,
          title: Text('Претендент на клибейт'),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context, false),
          )),
      body: Center(
        child: _url == null
            ? (_file != null
            ? Image.file(_file, fit: BoxFit.fill)
            : CircularProgressIndicator())
            : (Image.network(_url)),
      ),
      floatingActionButton: _file != null
          ? (FloatingActionButton(
        onPressed: () => _upload(context, _file),
        tooltip: 'Modify image',
        child: Icon(Icons.bubble_chart),
      ))
          : (_url != null
          ? FloatingActionButton(
        onPressed: () => Utils.share(_url),
        tooltip: 'Share image',
        child: Icon(Icons.share),
      )
          : null),
    );
  }

  Future _upload(BuildContext context, File image) async {
    setState(() {
      _file = null;
    });
    final directory = await getApplicationDocumentsDirectory();
    this._url = await compute(staticUpload, [directory.path, image]);
    setState(() {});
  }
}

Future<String> staticUpload(List<Object> params) async {
  print("Start staticUpload: ${params.toString()}");
  var tmpImage = img.copyResize(
      img.decodeImage((params[1] as File).readAsBytesSync()),
      width: 600);

  File file = Utils.createTmpFile((params[0] as String) + "/tmp.jpg");

  file.writeAsBytesSync(img.encodeJpg(tmpImage));

  var stream = new http.ByteStream(DelegatingStream.typed(file.openRead()));
  var length = await file.length();

  var uri = Uri.parse('${Utils.baseUrl}/upload');

  var request = new http.MultipartRequest("POST", uri);
  var multipartFile = new http.MultipartFile('file', stream, length,
      filename: basename(file.path));

  request.files.add(multipartFile);
  var response = await request.send();
  print(response.statusCode);
  String generatedUrl = await response.stream
      .transform(utf8.decoder)
      .first;
  print(generatedUrl);
  return generatedUrl;
}
