import 'dart:io';
import 'dart:typed_data';

import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/foundation.dart';
import 'package:url_launcher/url_launcher.dart';

class Utils {
  static final baseUrl = "https://photo-hack-server.herokuapp.com";

  static File createTmpFile(String filePath) {
    File file = new File(filePath);
    if (file.existsSync()) {
      file.deleteSync();
      print("Deleting $filePath file");
    }
    file.createSync();
    print("$filePath created: ${file.toString()}");
    return file;
  }

  static Future share(String url) async {
    var request = await HttpClient().getUrl(Uri.parse(url));
    var response = await request.close();
    Uint8List bytes = await consolidateHttpClientResponseBytes(response);
    await Share.file('PhotoHack', 'shakal.jpg', bytes, 'image/jpg',
        text: "Сделано на хакатоне PhotoHack");
  }

  static Future launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    }
  }
}
