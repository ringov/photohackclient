import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:transparent_image/transparent_image.dart';

import 'takepic.dart';
import 'utils.dart';

const String appName = "Shakal Adwords";

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: appName,
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.black,
        accentColor: const Color.fromARGB(255, 42, 164, 152),
      ),
      initialRoute: '/',
      routes: {'/': (context) => RootPage()},
      onGenerateRoute: (settings) {
        if (settings.name == ImageResultWidget.routeName) {
          final String arg = settings.arguments;
          return MaterialPageRoute(
            builder: (context) {
              return ImageResultWidget(arg);
            },
          );
        }
      },
    );
  }
}

class RootPage extends StatefulWidget {
  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  List<ItemWidget> list = [];

  var counter = 0;

  @override
  void initState() {
    super.initState();
    _loadFeed();
  }

  Future _loadFeed() async {
    print("Updating feed");
    Response response = await http.get("${Utils.baseUrl}/feed");
    List<String> values = json.decode(response.body).cast<String>();
    list.clear();
    for (var item in values) {
      list.add(ItemWidget(Key(item), item));
    }
    print("Feed updated");
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(appName)),
      body: RefreshIndicator(
        onRefresh: () async {
          await _loadFeed();
          return null;
        },
        child: ListView.builder(
            itemCount: list.length,
            itemBuilder: (context, index) => list[list.length - 1 - index]),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _showDialog(context),
        tooltip: 'Pick Image',
        child: Icon(Icons.add_a_photo),
      ),
    );
  }

  void _showDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("Выберите источник изображения"),
          actions: <Widget>[
            FlatButton(
              child: const Text("Camera"),
              onPressed: () => _getImageCamera(context),
            ),
            FlatButton(
              child: const Text("Gallery"),
              onPressed: () => _getImageGalley(context),
            ),
          ],
        );
      },
    );
  }

  Future _getImageCamera(BuildContext context) async {
    File image = await ImagePicker.pickImage(source: ImageSource.camera);
    if (image != null) {
      await Navigator.pushNamed(context, ImageResultWidget.routeName,
          arguments: image.path);
      Navigator.pop(context);
      _loadFeed();
    }
  }

  Future _getImageGalley(BuildContext context) async {
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      await Navigator.pushNamed(context, ImageResultWidget.routeName,
          arguments: image.path);
      Navigator.pop(context);
      _loadFeed();
    }
  }
}

class ItemWidget extends StatelessWidget {
  const ItemWidget(Key key, this.url) : super(key: key);

  final String url;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        GestureDetector(
          onTap: () => Utils.launchURL("https://hackathon.photolab.me"),
          child: Container(
            margin: EdgeInsets.all(8.0),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12.0),
              ),
              elevation: 8.0,
              child: Stack(
                children: <Widget>[
                  AspectRatio(
                      aspectRatio: 1.0,
                      child: Center(child: CircularProgressIndicator())),
                  ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    child: Center(
                      child: FadeInImage.memoryNetwork(
                        placeholder: kTransparentImage,
                        image: url,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Align(
          alignment: Alignment.topRight,
          child: Padding(
            padding: const EdgeInsets.all(24.0),
            child: IconButton(
                onPressed: () => Utils.share(url),
                icon: Icon(Icons.share, size: 32.0, color: Colors.white)),
          ),
        ),
      ],
    );
  }
}
